#!/bin/sh
set -e

if [ "$1" = 'server' ]; then
    gunicorn -b "[::]:80" -w 4 snoop.api
elif [ "$1" = 'migrate' ]; then
    python -m snoop.repository migrate
elif [ "$1" = 'worker' ]; then
    dramatiq snoop.tasks
else
    exec "$@"
fi

