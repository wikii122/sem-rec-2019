from snoop.repository import Job, Page


def job(job: Job):
    d = job._asdict()
    d["errors"] = list(d["errors"]) if d["errors"] else []
    d["page"] = None
    if job.finished_images and job.finished_page:
        d["status"] = "SUCCESS"
        d["page"] = f"/pages/{job.id}"
    elif job.errors:
        d["status"] = "ERROR"
    else:
        d["status"] = "PENDING"
    return d


def page(page: Page):
    d = page._asdict()
    d["images"] = list(d["images"]) if d["images"] else []
    return d

def page_summary(page: Page):
    d = page._asdict()
    d["images"] = list(d["images"]) if d["images"] else []
    del d["text"]
    return d