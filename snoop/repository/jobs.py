from typing import List, NamedTuple, Optional, Set

from . import scylla


class Job(NamedTuple):
    id: str
    url: str
    errors: Set[str]
    download_images: bool
    download_page: bool
    finished_images: bool = False
    finished_page: bool = False
    image_count: Optional[int] = None


def all() -> List[Job]:
    session = scylla.connect()
    job_cursor = session.execute(
        """
        SELECT id, url, errors, download_images, download_page, finished_images, finished_page, image_count
        FROM jobs
        """
    )
    return [Job(*args) for args in job_cursor]


def by_id(id: str) -> Optional[Job]:
    session = scylla.connect()
    job_cursor = session.execute(
        """
        SELECT id, url, errors, download_images, download_page, finished_images, finished_page, image_count
        FROM jobs 
        WHERE id = %s
        """,
        (id,),
    )

    data = list(job_cursor)
    if not data:
        return None

    return Job(*data[0])


def save(job: Job):
    """
    Save job data under given id. Creates row if required.
    """
    session = scylla.connect()
    session.execute(
        """
        INSERT INTO jobs (id, url, errors, download_images, download_page, finished_images, finished_page, image_count)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s)
        """,
        job,
    )


def add_error(id: str, message: str):
    """
    Add an image to page with given id.
    """
    session = scylla.connect()
    session.execute(
        """
        UPDATE jobs
        SET errors = errors + { %s }
        WHERE id = %s
        """,
        (message, id),
    )


CREATE_TABLE = """
    CREATE TABLE IF NOT EXISTS jobs (
        id text PRIMARY KEY,
        url text,
        errors set<text>,
        download_images boolean,
        download_page boolean,
        finished_images boolean,
        finished_page boolean,
        image_count int
    )
"""
