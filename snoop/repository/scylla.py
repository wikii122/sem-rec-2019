"""
Database initialization and support.

Uses cassandra driver to connect to Scylla instance.
"""
import os
from urllib.parse import urlsplit

import cassandra
from cassandra.cluster import Cluster

SCYLLA_URL = os.environ.get("SCYLLA_URL", "cql://localhost/snoop")

url = urlsplit(SCYLLA_URL)
hosts = url.hostname.split(",")
port = url.port or 9042
keyspace = url.path.lstrip("/")


def connect():
    """
    Creates new database session. 
    According to driver documentation, session should expire after leaving scope.
    """
    cluster = Cluster(
        hosts, port=port, load_balancing_policy=cassandra.policies.RoundRobinPolicy()
    )
    return cluster.connect(keyspace)
