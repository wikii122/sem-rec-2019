"""
Database manipulation options.

Currently supported:
- migrate - create schema on empty database.
"""
import logging
import sys
import time

import cassandra

from .utils import initialize_db

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger("snoop.repository")

    if sys.argv[1] == "migrate":
        logger.info("Attemping migration")

        try:
            initialize_db()
        except cassandra.cluster.NoHostAvailable:
            logger.warning("Cannot connect to scylla, exiting in a moment")
            # Sleep so system won't be flooded with request
            time.sleep(1)
            sys.exit(1)

        logger.info("Migration complete")
        logger.info("System is ready")
