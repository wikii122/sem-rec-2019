from typing import List, NamedTuple, Optional, Set

from . import scylla
from .utils import decode_url, encode_url


class Page(NamedTuple):
    id: str
    url: str
    text: Optional[str]
    images: Set[str]


def all() -> List[Page]:
    session = scylla.connect()
    job_cursor = session.execute(
        """
        SELECT id, url, text, images
        FROM pages
        """
    )
    return [Page(*args) for args in job_cursor]


def by_id(id: str) -> Optional[Page]:
    session = scylla.connect()
    job_cursor = session.execute(
        """
        SELECT id, url, text, images
        FROM pages
        WHERE id = %s
        """,
        (id,),
    )

    data = list(job_cursor)
    if not data:
        return None

    return Page(*data[0])


def add_image(id: str, image_url: str):
    """
    Add an image to page with given id.
    """
    session = scylla.connect()
    session.execute(
        """
        UPDATE pages
        SET images = images + { %s }
        WHERE id = %s
        """,
        (encode_url(image_url), id),
    )


def save(page: Page):
    """
    Store updated page object in database. Creates new object if it does not exist.
    """
    session = scylla.connect()
    session.execute(
        """
        INSERT INTO pages (id, url, text, images)
        VALUES (%s, %s, %s, %s)
        """,
        page,
    )


CREATE_TABLE = """
    CREATE TABLE IF NOT EXISTS pages (
        id text PRIMARY KEY,
        url text,
        text text,
        images set<text>
    )
"""
