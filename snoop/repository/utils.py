import base64

import cassandra
from cassandra.cluster import Cluster

from . import images, jobs, pages, scylla


def initialize_db():
    """
    Create keyspace and tables.
    """
    cluster = Cluster(scylla.hosts, port=scylla.port)
    session = cluster.connect()

    session.execute(
        f"""
        CREATE KEYSPACE IF NOT EXISTS {scylla.keyspace}
        WITH replication = {{'class': 'SimpleStrategy', 'replication_factor' : 2}}
        """
    )

    session.set_keyspace(scylla.keyspace)
    session.execute(jobs.CREATE_TABLE)
    session.execute(pages.CREATE_TABLE)
    session.execute(images.CREATE_TABLE)


def encode_url(url):
    return base64.urlsafe_b64encode(url.encode("utf-8")).decode("utf-8")


def decode_url(url):
    return base64.urlsafe_b64decode(url.encode("utf-8")).decode("utf-8")
