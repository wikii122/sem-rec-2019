from typing import List, Optional, Tuple

from . import pages, scylla
from .utils import decode_url, encode_url


def by_id(id: str) -> Optional[Tuple[str, bytearray]]:
    session = scylla.connect()
    name = decode_url(id)
    job_cursor = session.execute(
        """
        SELECT mimetype, image
        FROM images
        WHERE name = %s
        """,
        (name,),
    )

    data = list(job_cursor)
    if not data:
        return None

    return tuple(data[0])


def from_page(id: str) -> Optional[List[str]]:
    page = pages.by_id(id)
    if page is None:
        return None

    image_data = [
        {"url": decode_url(name), "identifier": name}
        for name in page.images
    ]

    return image_data


def save(name: str, mimetype: str, content: bytearray):
    """
    Save job data under given id. Creates row if required.
    """
    session = scylla.connect()
    session.execute(
        """
        INSERT INTO images (name, mimetype, image)
        VALUES (%s, %s, %s)
        """,
        (name, mimetype, content),
    )


CREATE_TABLE = """
    CREATE TABLE IF NOT EXISTS images (
        name text PRIMARY KEY,
        mimetype text,
        image blob
    )
"""
