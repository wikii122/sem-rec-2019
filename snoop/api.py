"""
Definition of application API.

Contains entrypoint for wsgi server, routing and each endpoint implementation.
"""
import uuid

import falcon
from falcon import Request, Response

from snoop import repository as repo
from snoop import views
from snoop.repository.jobs import Job
from snoop.tasks import download_url


class Resources:
    def on_get(self, req: Request, resp: Response):
        resp.media = {"jobs": "/jobs/", "pages": "/pages/"}


class Jobs:
    def on_get(self, req: Request, resp: Response):
        jobs = repo.jobs.all()
        resp.media = {"jobs": [views.job(job) for job in jobs]}

    def on_post(self, req: Request, resp: Response):
        data = req.media
        if not data["download_page"] and not data["download_images"]:
            resp.status = falcon.HTTP_BAD_REQUEST
            resp.media = {
                "error": "Cannot accept both download_page and download_images as false"
            }

            return

        idd = uuid.uuid4()

        job = Job(
            id=idd.hex,
            download_images=data["download_images"],
            download_page=data["download_page"],
            url=data["url"],
            errors=set(),
        )

        repo.jobs.save(job)
        download_url.send(job.id)

        resp.media = views.job(job)
        resp.status = falcon.HTTP_ACCEPTED


class JobDetails:
    def on_get(self, req: Request, resp: Response, job_id: str):
        job = repo.jobs.by_id(job_id)

        if job is None:
            resp.status = falcon.HTTP_NOT_FOUND
            resp.media = {"error": "Not found"}
            return

        resp.media = views.job(job)

        if job.finished_images and job.finished_page:
            resp.status = falcon.HTTP_CREATED
            resp.set_header("Location", f"/pages/{job.id}")


class Pages:
    def on_get(self, req: Request, resp: Response):
        pages = repo.pages.all()
        resp.media = {"pages": [views.page_summary(page) for page in pages]}


class PageDetails:
    def on_get(self, req: Request, resp: Response, page_id: str):
        page = repo.pages.by_id(page_id)

        if page is None:
            resp.status = falcon.HTTP_NOT_FOUND
            return

        resp.media = views.page(page)


class Images:
    def on_get(self, req: Request, resp: Response, page_id: str):
        images = repo.images.from_page(page_id)
        resp.media = {"images": images}


class ImageDetails:
    def on_get(self, req: Request, resp: Response, page_id: str, image_id: str):
        image = repo.images.by_id(image_id)
        if image is None:
            resp.status = falcon.HTTP_NOT_FOUND
            return

        resp.content_type = image[0]
        resp.data = image[1]


api = application = falcon.API()
api.add_route("/", Resources())
api.add_route("/jobs/", Jobs())
api.add_route("/jobs/{job_id}", JobDetails())
api.add_route("/pages/", Pages())
api.add_route("/pages/{page_id}", PageDetails())
api.add_route("/pages/{page_id}/images", Images())
api.add_route("/pages/{page_id}/images/{image_id}", ImageDetails())
