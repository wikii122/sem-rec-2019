from typing import Set
from urllib.parse import urljoin

import lxml
from lxml.html.clean import Cleaner

cleaner = Cleaner()
cleaner.javascript = True
cleaner.style = True


def extract_text(html: str) -> str:
    html_tree = lxml.html.fromstring(html)
    elements = cleaner.clean_html(html_tree)
    texts = elements.itertext()

    return "".join(texts)


def extract_image_urls(base_url: str, html: str) -> Set[str]:
    html_tree = lxml.html.fromstring(html)
    urls = html_tree.xpath("//img/@src")
    return {urljoin(base_url, url) for url in urls}
