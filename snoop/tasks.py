import logging
import os
from typing import List, Optional, Set, Tuple

import dramatiq
import requests
from dramatiq.brokers.rabbitmq import RabbitmqBroker
from dramatiq.brokers.stub import StubBroker

from snoop import html
from snoop.repository import Job, Page, images, jobs, pages

rabbit_host = os.environ.get("RABBITMQ_URL")
if rabbit_host:
    broker = RabbitmqBroker(
        host=rabbit_host,
        retry_delay=5,
        connection_attempts=100,
        heartbeat_interval=600,
        blocked_connection_timeout=300,
    )
else:
    logging.error(
        "THIS IS AN ERROR ON PRODUCTION! No RABBITMQ_URL provided, using stub broker for messaging."
    )
    broker = StubBroker()
    broker.emit_after("process_boot")

dramatiq.set_broker(broker)


@dramatiq.actor(max_retries=5)
def download_url(idd: str):
    """
    Download and process page, then queue download for each image if required.
    """
    job = jobs.by_id(idd)
    if job is None:
        logging.error("Unknown job id")
        return

    try:
        logging.info(f"Processing job {idd}. Downloading url {job.url}")
        response = requests.get(job.url)
        response.raise_for_status()
        page, job, image_urls = process_page(job, response)
    except requests.RequestException as e:
        jobs.add_error(idd, str(e))
        raise  # Allow worker to handle failed job according to config

    jobs.save(job)
    if page:
        pages.save(page)

    if job.download_images:
        for image_url in image_urls:
            download_image.send(idd, image_url)


def process_page(
    job: Job, response: requests.Response
) -> Tuple[Optional[Page], Job, Set[str]]:
    text = html.extract_text(response.text) if job.download_page else None
    image_urls = html.extract_image_urls(job.url, response.text)

    page = Page(id=job.id, url=job.url, text=text, images=set())
    job = job._replace(image_count=len(image_urls), finished_page=True)
    if not image_urls:
        job = job._replace(finished_images=True)

    return page, job, image_urls


@dramatiq.actor(max_retries=5)
def download_image(idd: str, image_url: str):
    """
    Download image and update job and image list with it.
    """
    logging.info(f"Processing image for job {idd}. Downloading image {image_url}")
    try:
        response = requests.get(image_url)
        response.raise_for_status()
        datatype = response.headers.get("Content-Type")
        content = response.content
    except requests.RequestException as e:
        jobs.add_error(idd, str(e))
        raise  # Allow worker to handle failed job according to config

    images.save(image_url, datatype, content)
    pages.add_image(idd, image_url)

    page = pages.by_id(idd)
    job = jobs.by_id(idd)
    if job.image_count == len(page.images):
        job = job._replace(finished_images=True)
        jobs.save(job)
