from snoop.html import extract_image_urls, extract_text


def test_text_extraction_from_tags():
    text1 = "lalala"
    text2 = "trelele"
    html = f"<html><div>{text1}</div>{text2}</html>"
    assert extract_text(html) == f"{text1}{text2}"


def test_text_extraction_ignores_scripts():
    text1 = "lalala"
    text2 = "trelele"
    text3 = "nanana"
    html = f"<html><script>{text3}</script><div>{text1}</div>{text2}</html>"
    assert extract_text(html) == f"{text1}{text2}"


def test_text_extraction_ignores_styles():
    text1 = "lalala"
    text2 = "trelele"
    text3 = "nanana"
    html = f"<html><style>{text3}</style><div>{text1}</div>{text2}</html>"
    assert extract_text(html) == f"{text1}{text2}"


def test_image_url_absolute_extraction():
    site = "http://example.com/bee.png"
    link = "http://example.com/about.html"

    html = f"<html><img src='{site}'><div>text1</div>text2</html>"
    assert extract_image_urls(link, html) == {f"{site}"}

    html = f"<html><img src='/bee.png'><div>text1</div>text2</html>"
    assert extract_image_urls(link, html) == {f"{site}"}


def test_image_url_relative_extraction():
    site = "http://example.com/bee.png"
    link = "http://example.com/about.html"

    html = f"<html><img src='./bee.png'><div>text1</div>text2</html>"
    assert extract_image_urls(link, html) == {f"{site}"}

    html = f"<html><img src='bee.png'><div>text1</div>text2</html>"
    assert extract_image_urls(link, html) == {f"{site}"}
