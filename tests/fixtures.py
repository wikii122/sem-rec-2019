import pytest
import responses
from falcon import testing

from snoop.api import api
from snoop.repository import Job, Page


@pytest.fixture
def client():
    return testing.TestClient(api)


@pytest.fixture
def requests_mock():
    with responses.RequestsMock() as rsps:
        yield rsps


@pytest.fixture
def page():
    return Page(id="asd", url="http://test.test", text="Lorem ipsum", images=set())


@pytest.fixture
def job():
    return Job(
        id="asd",
        download_images=True,
        download_page=True,
        url="http://test.test",
        errors=set(),
    )
