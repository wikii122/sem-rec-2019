import pytest
import requests

from snoop.tasks import process_page

from .fixtures import job, requests_mock


def test_page_processing(job, requests_mock):
    text = "asdfgh"
    requests_mock.add(
        requests_mock.GET,
        job.url,
        body=f"<div>{text}</div>",
        status=200,
        content_type="application/json",
    )
    response = requests.get(job.url)
    page, result_job, images = process_page(job, response)

    assert result_job.image_count == 0
    assert page.text == text
    assert page.url == job.url
    assert result_job.id == job.id
    assert result_job.finished_page
    assert result_job.finished_images
    assert len(images) == 0


def test_page_processing_with_images(job, requests_mock):
    text = "asdfgh"
    link1 = "/i.png"
    requests_mock.add(
        requests_mock.GET,
        job.url,
        body=f"<img src='{link1}'><div>{text}</div><img src='{link1}'>",
        status=200,
        content_type="application/json",
    )
    response = requests.get(job.url)
    page, result_job, images = process_page(job, response)

    assert result_job.image_count == 1
    assert page.text == text
    assert page.url == job.url
    assert result_job.id == job.id
    assert result_job.finished_page
    assert not result_job.finished_images
    assert len(images) == 1
