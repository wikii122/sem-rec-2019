"""
Testing api calls, mostly if responses are correct.
"""
import json

import falcon
import pytest

from snoop import views
from snoop.repository.jobs import Job

from .fixtures import client, job, page


def test_root_response_ok(client):
    response = client.simulate_get("/")
    assert response.status == falcon.HTTP_OK


def test_jobs_list(client, monkeypatch, job):
    monkeypatch.setattr("snoop.repository.jobs.all", lambda: [job])

    response = client.simulate_get("/jobs")

    assert response.status == falcon.HTTP_OK
    data = json.loads(response.content)
    assert len(data["jobs"]) == 1
    assert data["jobs"][0] == views.job(job)


def test_create_job(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.jobs.save", lambda x: None)

    response = client.simulate_post(
        "/jobs/",
        body=json.dumps(
            {"url": "http://test.test", "download_page": True, "download_images": True}
        ),
    )

    assert response.status == falcon.HTTP_ACCEPTED
    data = json.loads(response.content)
    assert data["id"] is not None
    assert data["download_page"]
    assert data["download_images"]


def test_create_job_no_download(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.jobs.save", lambda x: pytest.fail())
    response = client.simulate_post(
        "/jobs/",
        body=json.dumps(
            {
                "url": "http://test.test",
                "download_page": False,
                "download_images": False,
            }
        ),
    )

    assert response.status == falcon.HTTP_BAD_REQUEST
    data = json.loads(response.content)
    assert data.get("error") is not None


def test_jobs_not_found(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.jobs.by_id", lambda x: None)

    response = client.simulate_get("/jobs/asd")
    assert response.status == falcon.HTTP_NOT_FOUND


def test_jobs_found(client, monkeypatch, job):
    monkeypatch.setattr("snoop.repository.jobs.by_id", lambda x: job)

    response = client.simulate_get("/jobs/asd")
    assert response.status == falcon.HTTP_OK
    data = json.loads(response.content)
    assert data == views.job(job)


def test_jobs_done(client, monkeypatch, job):
    job = job._replace(finished_page=True, finished_images=True)
    monkeypatch.setattr("snoop.repository.jobs.by_id", lambda x: job)

    response = client.simulate_get("/jobs/asd")
    assert response.status == falcon.HTTP_CREATED
    assert response.headers.get("Location") is not None
    data = json.loads(response.content)
    assert data == views.job(job)


def test_pages_list(client, monkeypatch, page):
    monkeypatch.setattr("snoop.repository.pages.all", lambda: [page])

    response = client.simulate_get("/pages")

    assert response.status == falcon.HTTP_OK
    data = json.loads(response.content)
    assert data["pages"] == [views.page_summary(page)]


def test_page_not_found(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.pages.by_id", lambda x: None)

    response = client.simulate_get("/pages/id")
    assert response.status == falcon.HTTP_NOT_FOUND


def test_page_found(client, monkeypatch, page):
    monkeypatch.setattr("snoop.repository.pages.by_id", lambda x: page)

    response = client.simulate_get("/pages/id")
    assert response.status == falcon.HTTP_OK
    data = json.loads(response.content)
    assert data == views.page(page)


def test_images_list(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.images.from_page", lambda x: [])

    response = client.simulate_get("/pages/id/images")

    assert response.status == falcon.HTTP_OK
    data = json.loads(response.content)
    assert data["images"] == []


def test_image_not_found(client, monkeypatch):
    monkeypatch.setattr("snoop.repository.images.by_id", lambda x: None)

    response = client.simulate_get("/pages/id/images/is")
    assert response.status == falcon.HTTP_NOT_FOUND


def test_image_found(client, monkeypatch):
    monkeypatch.setattr(
        "snoop.repository.images.by_id", lambda x: ("image/png", bytes())
    )

    response = client.simulate_get("/pages/id/images/is")
    assert response.status == falcon.HTTP_OK
    assert response.headers.get("Content-Type") == "image/png"
