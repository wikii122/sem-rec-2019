# Snoop

## Dependencies

Dependencies are managed by [Poetry](https://poetry.eustace.io/).

## Running

Program requires to run two env variables to be set, containing correspondent services URIs - RABBITMQ_URL and SCYLLADB_URL.

Perform schema migration with `python -m snoop.repository migrate`

Start server with `gunicorn snoop.api`

Start worker with `dramatiq snoop.tasks`

Or simply run `docker-compose up`

## Design

This repository contains server and asynchronous worker code, they should be linked together using RabbitMQ message queue.
This separation was needed, as page downloads may take a long time and thus should be performed in the background. Page download and image download are both asynchronous jobs performed on the worker. 

Data persistence depends on ScyllaDB column database. It was chosen because in this task data structures involved seem way too simple to arrange a fully fledged relational database and it also allows for far more scalability and availability when necessary.

It is also worth to note that images should be stored in some external storage, like S3, but I wanted this repo to be able to start service out of the box and so did not want to enter contain AWS credentials in it, so I decided to create another table for images. In real product, it should definitely be moved to S3.

The decision to not track download image tasks separately for each image but to treat download order as a whole was to avoid a situation when one remaining task left becomes suddenly hundreds (when images were added), which I found very unintuitive. If these assumptions were to change, in order to accommodate this change, both the data model and API would need to be modified as well.

## API

Endpoint GET */jobs/*
Used to list jobs.

Response 200 OK

        {
            "jobs": [
                {
                "id": "405aad60b39e4d23a1ff7557da596994",
                "url": "http://wiktorsleczka.com/test01",
                "errors": [],
                "download_images": true,
                "download_page": true,
                "finished_images": true,
                "finished_page": true,
                "image_count": 5,
                "page": "/pages/405aad60b39e4d23a1ff7557da596994",
                "status": "SUCCESS"
                }
            ]
        }

Endpoint POST */jobs/*
Used to create jobs.
Payload:

    {
        "url": "http://wiktorsleczka.com/test01",
        "download_images": true,
        "download_page": true
    }

Response 202 Accepted:

    {
        "id": "405aad60b39e4d23a1ff7557da596994",
        "url": "http://wp.pl",
        "errors": [],
        "download_images": true,
        "download_page": true,
        "finished_images": false,
        "finished_page": false,
        "image_count": 5,
        "status": "PENDING"
    }

Endpoint GET */jobs/{job_id}*
Shows job details
Responses:
200 OK on pending job or download errors 

    {
        "id": "405aad60b39e4d23a1ff7557da596994",
        "url": "http://wiktorsleczka.com/test01",
        "errors": [],
        "download_images": true,
        "download_page": true,
        "finished_images": false,
        "finished_page": true,
        "image_count": 5,
        "status": "PENDING"
    }

201 Created with Location header set on success

    {
        "id": "405aad60b39e4d23a1ff7557da596994",
        "url": "http://wiktorsleczka.com/test01",
        "errors": [],
        "download_images": true,
        "download_page": true,
        "finished_images": true,
        "finished_page": true,
        "image_count": 5,
        "page": "/pages/405aad60b39e4d23a1ff7557da596994",
        "status": "SUCCESS"
    }

Endpoint GET */pages/*
List summary of all pages
Response 200 OK

    {
    "pages": [
        {
            "id": "405aad60b39e4d23a1ff7557da596994",
            "url": "http://wiktorsleczka.com/test01",
            "images": [ "a1", "a2", "a3", "a4", "a5"]
        }
    }

Endpoint GET */pages/{page_id}*
Shows page
Response 200 OK

        {
            "id": "405aad60b39e4d23a1ff7557da596994",
            "url": "http://wiktorsleczka.com/test01",
            "images": [ "a1", "a2", "a3", "a4", "a5"],
            "text": "Lorem ipsum"
        }

Endpoint GET */pages/{page_id}/images*
List images with urls they were downloaded from
Response 200 OK

    {
        "images": {
            "identifier": "a1",
            "url": "wiktorsleczka.com/img/a1"
        }
    }


Endpoint GET */pages/{page_id}/images/{image_id}*
Show image (binary)
Response 200 OK

## Testing

All tests are run with `pytest` command from the root directory. Tests check almost all logic involved in service and also most API responses with mocked data.

Database/repository module is not tested. It should be tested with an integrated test, but due to a really long database startup in Docker, small number and simplicity of queries I found it to be not practical. However, when having a standby database instance on staging/test environment, such test should be added, as should be added a few end to end tests.

## Possible improvements

- Moving image store to S3
- Through testing - the project is missing e2e and integration tests due to long database start time, which is necessary when running tests with docker compose. The recommended solution is to add optional tests that run on a staging/test environment.
- Overall startup time - Compilation of some dependencies and database startup take a very long time. Recommended solution - change the setup to use an external database and prebuild docker image with dependencies installed.
- Error handling - there are some edge cases that need improvement, for instance when given the nonexistent domain, the errors list is populated with multiple version of the same error. I also don't like that docker compose must restart all services until database and message broker start to work, it should be made in a more graceful way.
- Pagination on lists - if service is used often, the lists may grow very long and thus cause performance problems.