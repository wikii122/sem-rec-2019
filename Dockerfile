FROM python:3.7.2-alpine

MAINTAINER Wiktor Ślęczka <wiktor.sleczka@gmail.com>

RUN apk add --no-cache gcc python3-dev musl-dev libxml2-dev libxslt-dev
RUN pip install 'poetry==0.12.11'
RUN poetry config settings.virtualenvs.create false

RUN mkdir /code
WORKDIR /code

COPY pyproject.toml .
RUN poetry install --no-dev

COPY entrypoint.sh .
RUN chmod 0755 entrypoint.sh

COPY snoop/ snoop/

EXPOSE 80

ENTRYPOINT ["/code/entrypoint.sh"]
CMD ["server"]
